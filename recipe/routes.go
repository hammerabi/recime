package recipe

import (
	"github.com/gin-gonic/gin"
)

type getResponse struct {
	Data       []Recipe `json:"data"`
	TotalCount int      `json:"totalCount"`
}

func HandlePOST(repo RecipeRepository) gin.HandlerFunc {
	return func(c *gin.Context) {
		var recipe Recipe
		err := c.BindJSON(&recipe)
		if err != nil {
			c.AbortWithStatusJSON(400, gin.H{"message": err.Error()})
		}
		if recipe.Name == "" || recipe.Description == "" || recipe.Instructions == "" {
			c.AbortWithStatusJSON(400, gin.H{"message": "Some recipe fields are null"})
		}
		repo.CreateRecipe(&recipe)
	}
}

func HandleGET(repo RecipeRepository) gin.HandlerFunc {
	return func(c *gin.Context) {
		recipes := repo.GetIngredients()
		response := &getResponse{
			Data:       recipes,
			TotalCount: len(recipes),
		}

		c.JSON(200, response)
	}
}
