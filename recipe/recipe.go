package recipe

func (r *RecipeRepository) CreateRecipe(recipe *Recipe) {
	r.DBClient.Create(&recipe)
}

func (r *RecipeRepository) GetIngredients() []Recipe {
	var recipes []Recipe
	r.DBClient.Find(&recipes)

	return recipes
}
