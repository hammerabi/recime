package recipe

import "gorm.io/gorm"

type RecipeAccesser interface {
	CreateRecipe()
	GetIngredients()
}

// Recioe is a temporary struct until the repository patter is established
type Recipe struct {
	gorm.Model
	RecipeID     uint64 `json:"recipe_id,omitempty" format:"uint64" name:"recipe_id"`
	Name         string `json:"name" format:"string" name:"name"`
	Description  string `json:"description" format:"string" name:"description"`
	Instructions string `json:"instructions" format:"string" name:"instructions"`
}

type RecipeRepository struct {
	DBClient *gorm.DB
}
