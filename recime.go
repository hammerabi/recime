package main

import (
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"gitlab.com/hammerabi/recime/database"
	"gitlab.com/hammerabi/recime/recipe"
)

var router *gin.Engine

const (
	host     = "localhost"
	port     = 5432
	user     = "joshua.butcher"
	password = ""
	dbname   = "recimedb"
)

func main() {

	dbConfig := &database.DatabaseConfig{
		Host:     host,
		Port:     port,
		User:     user,
		Password: password,
		DbName:   dbname,
	}

	db, err := database.InitDB(dbConfig)
	recipeRepository := &recipe.RecipeRepository{
		DBClient: db,
	}
	CheckError(err)

	router = gin.New()
	router.UseRawPath = true
	router.ForwardedByClientIP = true
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PATCH", "DELETE"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	routerGroup := router.Group("/api/")
	routerGroup.POST("/recipes", recipe.HandlePOST(*recipeRepository))
	routerGroup.GET("/recipes", recipe.HandleGET(*recipeRepository))

	router.Run(":7777")

}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
