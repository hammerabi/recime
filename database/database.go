package database

import (
	"fmt"

	"gitlab.com/hammerabi/recime/recipe"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DatabaseConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DbName   string
}

func InitDB(config *DatabaseConfig) (*gorm.DB, error) {
	psqlconn := fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s sslmode=disable",
		config.Host,
		config.Port,
		config.User,
		config.DbName,
	)

	// open database
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  psqlconn,
		PreferSimpleProtocol: true, // disables implicit prepared statement usage
	}), &gorm.Config{})

	if err != nil {
		return nil, err
	}

	db.AutoMigrate(&recipe.Recipe{})

	return db, nil
}
